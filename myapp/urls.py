"""
URL configuration for myapp project.

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from ui.views import *

urlpatterns = [
    # path('admin/', admin.site.urls),
    path('', home, name='home'),
    path('du-an', Projects, name='projects'),
    path('du-an/<slug>', ProjectsDetail, name='project-detail'),
    path('tin-tuc', Blog, name="blog"),
    path('tin-tuc/<slug>', BlogDetail, name="blog-detail"),
    path('tuyen-dung', Recruitments, name='recruitments'),
    path('tuyen-dung/<slug>', RecruitmentsDetail, name='recruitment-detail'),
    path('admin/banner', AdminBanner, name='admin-banner'),
    path('admin/banner/<id>', AdminDeleteBanner, name='admin-banner-delete'),
    path('admin/detail/banner/<id>', AdminBannerDetail, name='admin-banner-detail'),
    path('admin/update/banner/<id>', AdminEditBanner, name='admin-banner-edit'),
    path('admin/about', AdminAbout, name='admin-about'),
    path('admin/about/<id>', AdminAboutDetail, name='admin-about-detail')
]
