const slidesContainer = document.querySelectorAll(".slides-container");
const prevButton = document.querySelectorAll(".prev");
const nextButton = document.querySelectorAll(".next");

nextButton.forEach((element, index) => {

	element.addEventListener("click", () => {
		slidesContainer[index].scrollLeft += slidesContainer[index].querySelector(".slide").clientWidth;
	});
})


prevButton.forEach((element, index) => {

	element.addEventListener("click", () => {
		slidesContainer[index].scrollLeft -= slidesContainer[index].querySelector(".slide").clientWidth;
	});
})
