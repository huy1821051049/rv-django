from django.db import models

# Create your models here.
class Logo(models.Model):
    name = models.TextField()

class Banner(models.Model):
    PAGE = [
        ("HOME", "HOME"),
        ("ABOUT", "ABOUT"),
        ("ACTIVITY", "ACTIVITY"),
        ("PROJECTS", "PROJECTS"),
        ("NEWS", "NEWS"),
        ("RECRUITMENT", "RECRUITMENT"),
        ("CONTACT", "CONTACT")
    ]
    image = models.TextField()
    page = models.CharField(max_length=15, choices=PAGE)


class About(models.Model):
    short_title = models.TextField(max_length=20)
    title = models.TextField(max_length=20)
    content = models.TextField()
    image = models.TextField()


class Ativity(models.Model):
    short_title = models.TextField(max_length=20)
    title = models.TextField(max_length=20)
    image = models.TextField()

class ItemActivity(models.Model):
    svg = models.TextField()
    title = models.TextField(max_length=25)
    short_content = models.TextField(max_length=50)
    content = models.TextField()


class DescProjects(models.Model):
    short_title = models.CharField(max_length=10)
    title = models.CharField(max_length=50)

class ItemProjects(models.Model):
    image = models.TextField()
    title = models.CharField(max_length=50)
    area = models.CharField(max_length=50)
    content = models.CharField(max_length=50)
    slug = models.TextField(default='')
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

class Recruitment(models.Model):
    image = models.TextField()
    short_title = models.CharField(max_length=25)
    title = models.CharField(max_length=25)

class Item(models.Model):
    title = models.CharField(max_length=50, null=False)
    salary = models.CharField(max_length=25)
    quantity = models.IntegerField()
    area = models.CharField(max_length=100)
    level = models.CharField(max_length=50)
    form = models.CharField(max_length=50)
    expiration_date = models.DateField()
    slug = models.TextField()
    content = models.TextField()
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

class DescNews(models.Model):
    short_title = models.CharField(max_length=20)
    title = models.CharField(max_length=50)

class ItemNews(models.Model):
    title = models.CharField(max_length=50)
    short_content = models.CharField(max_length=100)
    content = models.TextField()
    image = models.TextField()
    created_by = models.CharField(max_length=20)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

class DescParner(models.Model):
    title = models.CharField(max_length=25, null=False, blank=False)
    short_content = models.CharField(max_length=50, null=False, blank=False)
    status = models.BooleanField(default=True)

class ItemPartner(models.Model):
    image = models.TextField()
    link = models.TextField()
    status = models.BooleanField(default=True)









