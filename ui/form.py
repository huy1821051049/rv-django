from django.core.files.storage import FileSystemStorage
from datetime import datetime


def UploadFile(file):
    current_time = datetime.now()
    milliseconds = current_time.timestamp() * 1000
    fs = FileSystemStorage()
    filename = fs.save(str(milliseconds) + '-' + file.name, file)
    uploaded_file_url = fs.url(filename)
    return "/ui" + uploaded_file_url