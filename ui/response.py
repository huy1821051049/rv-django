from django.http import HttpResponse
import json

def ResponseObjectSuccess(data):
    data = {
        "status": 200,
        "data": data,
        "message": "thành công"
    }
    
    json_data = json.dumps(data)
    return  HttpResponse(json_data, content_type='application/json')