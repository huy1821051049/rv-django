from django.shortcuts import render
from django.core.paginator import Paginator
from .models import *
from django.http import HttpResponseRedirect, HttpResponse
from .form import *
from .response import *
from django.db.models import Q

# Create your views here.


def home(request):

    banners = Banner.objects.filter(page="HOME").values()
    abouts = About.objects.all().first()
    activities = Ativity.objects.all().first()
    itemActivity = ItemActivity.objects.all().values()
    descProjects = DescProjects.objects.all().first()
    itemProjects = ItemProjects.objects.all()
    recruitment = Recruitment.objects.all().first()
    itemRecruitment = Paginator(Item.objects.all(), 3).get_page(1)
    descNews = DescNews.objects.all().first()
    itemNews = Paginator(ItemNews.objects.all(), 3).get_page(1)
    descPartner = DescParner.objects.all().first()
    itemPartner = ItemPartner.objects.all().filter(status=1)
    return render(request, "home-page/index.html", context={'banners': banners, 'abouts': abouts, 'activities': activities, 'itemActivity': itemActivity, 'descProjects': descProjects, 'itemProjects': itemProjects, 'recruitment': recruitment, 'itemRecruitment': itemRecruitment, 'descNews': descNews, 'itemNews': itemNews, 'descPartner': descPartner, 'itemPartner': itemPartner})


def Projects(request):

    itemProjects = ItemProjects.objects.all()
    return render(request, "projects/index.html", context={'itemProjects': itemProjects})


def ProjectsDetail(request, slug):
    projectDetail = ItemProjects.objects.all().filter(id=slug).first()
    relatedProjects = Paginator(
        ItemProjects.objects.all().exclude(id__exact=slug), 3).get_page(1)
    print(relatedProjects)
    return render(request, "projects/detail.html", context={"projectDetail": projectDetail, "relatedProjects": relatedProjects})


def Blog(request):
    return render(request, 'blog/index.html')


def BlogDetail(request, slug):
    return render(request, 'blog/detail.html')


def Recruitments(request):
    return render(request, "recruitments/index.html")


def RecruitmentsDetail(request, slug):
 
    return render(request, "recruitments/detail.html")


def AdminBanner(request):
    if request.method == "GET":
        print(request.GET.get('page'))
        page= request.GET.get('page')
        banner = []
        if page: 
            banner = Paginator(Banner.objects.all().filter(Q(page__iexact=page)).order_by("-id"), 10).get_page(1)
        else:
            banner = Paginator(Banner.objects.all().order_by("-id"), 10).get_page(1)
        
        return render(request, "admin/banner/index.html", {'banner': banner})

    if request.method == "POST":
        file = request.FILES['image']
        uploaded_file_url = UploadFile(file)

        image = uploaded_file_url
        page = request.POST['page']

        banner = Banner(image=image,page=page)
        banner.save()

        return HttpResponseRedirect("/admin/banner")

def AdminBannerDetail(request, id):
    banner = Banner.objects.get(id=id)
    res = {
        "id": banner.id,
        "image": banner.image,
        "page": banner.page
    }
    response = ResponseObjectSuccess(res)
    return response

def AdminDeleteBanner(request, id):
    Banner.objects.get(id=id).delete()
    return HttpResponseRedirect("/admin/banner")


def AdminEditBanner(request, id):
    file = request.FILES['image']
    page = request.POST['page']
    banner = Banner.objects.get(id=id)

    if file:
        uploaded_file_url = UploadFile(file)
        banner.image = uploaded_file_url


    if page:
        banner.page = page

    banner.save()

    return HttpResponseRedirect("/admin/banner")

def AdminAbout(request):
    if request.method == "GET":
        abouts = About.objects.all()
        return render(request, "admin/about/index.html", {'abouts': abouts})
    
def AdminAboutDetail(request, id):
    about = About.objects.get(id=id)
    if about:
        res = {
            "id": about.id,
            "image": about.image,
            "title": about.title,
            "short_title": about.short_title,
            "content": about.content
        }
        about = ResponseObjectSuccess(res)

    return about

   
